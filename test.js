function getWord(template) {
  let words = [];
  let start, end;

  do {
    start = template.indexOf("{")
    end = template.indexOf("}", start);
    if (start > -1) {
      words.push(template.substr(start+1,end-start-1));

      template = template.substr(end+1);
    }
  } while(start > -1);

  return words;
}

function replace(template, value) {
  let result = template;

  getWord(template).forEach(function(item) {
    result = result.replace(`{${item}}`,value[item]);
  });

  return result;
}

const template1 = "Check out {brand_name}'s [Twitter Page]({twitter_url})!";
const values1 = {
    brand_name: "Nike",
    twitter_url: "https://twitter.com/nike",
};

const template2 = "Buy {product_name} by {brand_name} on [Snagshout]({link})";
const values2 = {
    brand_name: "Nike",
    link: "http://snag.it/322651",
    product_name: "Demo Product",
};

console.log(replace(template1,values1));
console.log(replace(template2,values2));
